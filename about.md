---
layout: page
title: About
permalink: /about/
---

Every Sunday night at our house pizza night. It came from when I was a boy, and we had pizza night on the weekends as well. Part of continuing this tradition is to remember my mom. And the rest is because pizza is frankly, irresistible. 

Most of these recipes do not use tomato sauce. That's a personal preference, both in part because the acidity of tomato sauce doesn't sit well with me and tomato sauce is the goto sauce for every pizza. And these are not your normal pizzas. They're also not your gourmet pizzas. With kids running around the house I'll cut corners where needed if the end result is similarly delicious. 